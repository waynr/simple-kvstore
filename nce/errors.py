# -*- coding:utf-8 -*-
"""Defines application-wide exceptions."""


class KVStoreError(Exception):
    """Base exception for all other kvstore-specifc exceptions."""
    pass


class ConfigurationError(KVStoreError):
    """Indicates problem with application settings."""
    pass
