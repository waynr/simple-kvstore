# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""

from flask import Flask

from nce.api import endpoints
from nce.api import v1
from nce.settings import ProdConfig


def create_app(config_object=ProdConfig):
    """An application factory, as explained here:
    http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config_object)

    # register blueprints
    app.register_blueprint(endpoints.blueprint)
    app.register_blueprint(v1.blueprint)

    return app
