# -*- coding: utf-8 -*-
"""Contains an abstract factory for DataStore implementations."""

from flask import Config

from nce.datastore.base import DataStore
from nce.datastore.errors import InvalidDataStoreError
from nce.errors import ConfigurationError


class DataStoreFactory(object):
    """Datastore clients should not hard code the DataStore implementation they
    are using. This factory facilitates that abstraction by providing method(s)
    to produce concrete DataStore subclass objects.
    """

    @staticmethod
    def create_from_flask_config(flask_config):
        """Given flask app config, create and return a DataStore subclass
        object based on the class reference stored in the config.
        """
        if isinstance(flask_config, Config):
            ds_class = flask_config.get('DATASTORE_CLASS', None)
        else:
            raise ConfigurationError("Invalid configuration type.")

        if ds_class is None:
            raise ConfigurationError("Missing 'DATASTORE_CLASS' config "
                                     "setting.")

        if not issubclass(ds_class, DataStore):
            raise InvalidDataStoreError

        return ds_class(flask_config)
