# -*- coding: utf-8 -*-
"""Define a DataStore subclass for dynamodb.
"""

import boto3
import botocore
from nce.datastore.base import DataStore


class DynamoDataStore(DataStore):
    """
    DynamoDB implementation of DataStore.
    """

    kvstore_table_definition = {
        'TableName': 'kvstore',
        'KeySchema': [
            {
                'AttributeName': 'key',
                'KeyType': 'HASH'
            },
        ],
        'AttributeDefinitions': [
            {
                'AttributeName': 'key',
                'AttributeType': 'S'
            },
        ],
        'ProvisionedThroughput': {
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5,
        }
    }

    def __init__(self, *args):
        super().__init__(*args)

        self.initialized = False
        self.aws_region = self.config.get('AWS_REGION', 'us-west-2')

        # Need to determine if we should use dynamodb and what host/port to
        # point at.
        self._use_local = self.config.get('DYNAMODB_LOCAL', False)
        self._local_host = self.config.get('DYNAMODB_LOCAL_HOST', None)
        self._local_port = self.config.get('DYNAMODB_LOCAL_PORT', 8000)

        service_kwargs = {
            'service_name': 'dynamodb',
            'region_name': self.aws_region,
        }

        if self._use_local:
            service_kwargs['endpoint_url'] = "http://{0}:{1}".format(
                self._local_host, self._local_port)

        self._service = boto3.resource(**service_kwargs)
        self._client = boto3.client(**service_kwargs)

        alternate_tablename = self.config.get('DYNAMODB_TABLENAME', None)
        if alternate_tablename is not None:
            self.kvstore_table_definition['TableName'] = alternate_tablename

        if not self.initialized:
            self.setup()
            self.initialized = True

        self.__ddb_table = None

    @property
    def ddb_tablename(self):
        return self.kvstore_table_definition['TableName']

    @property
    def ddb_table(self):
        if self.__ddb_table is None:
            self.__ddb_table = self._service.Table(self.ddb_tablename)
        return self.__ddb_table

    def setup(self):
        try:
            self._service.create_table(**self.kvstore_table_definition)
        except botocore.exceptions.ClientError:
            # If table already exists, let's try using it.
            pass

        # Wait for table to exist to prevent race conditions.
        waiter = self._client.get_waiter('table_exists')
        waiter.wait(TableName=self.ddb_tablename, WaiterConfig={'Delay': 1})

    def teardown(self):
        self.ddb_table.delete()

        # Wait for table to stop existing to prevent race conditions.
        waiter = self._client.get_waiter('table_not_exists')
        waiter.wait(TableName=self.ddb_tablename, WaiterConfig={'Delay': 1})
        self.__ddb_table = None

    def add(self, kvs):
        for k, v in kvs.items():
            self.ddb_table.put_item(Item={'key': k,
                                          'value': v})

    def get(self, keys=None):
        result = {}

        if not keys:
            # return all keys from DynamoDB
            response = self.ddb_table.scan()

            # TODO: Handle pagination behavior of scan method for results > 1MB
            # in size. See
            # https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Scan.html#Scan.Pagination
            for item in response["Items"]:
                result[item['key']] = item['value']
        else:
            for key in keys:
                ddb_item = self.ddb_table.get_item(
                    Key={'key': key}).get('Item', None)
                if ddb_item:
                    result[key] = ddb_item['value']
                else:
                    result[key] = None
        return result

    def update(self, kvs):
        for k, v in kvs.items():
            self.ddb_table.update_item(Key={'key': k},
                                       AttributeUpdates={
                                           "value": {
                                               "Action": "ADD",
                                               "Value": v,
                                           }
                                       })

    def delete(self, keys):
        for k in keys:
            print(k)
            self.ddb_table.delete_item(Key={'key': k})
