# -*- coding: utf-8 -*-
"""Public flask views."""

import abc


class DataStore(abc.ABC):
    """
    Define abstract interface for key value backing data store to enable
    additional datastore implementations in the future. This makes it easier to
    switch between datastores based on a config value.
    """

    def __init__(self, flask_config):
        self.config = flask_config

    @abc.abstractmethod
    def add(self, keys):
        """
        Add given dictionary to datastore.

        :param dict keys: Dictionary where keys correspond to datastore keys
        and values are lists of strings to be associated with each key in the
        datastore.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, keys):
        """
        Retrieve listed keys from datastore.

        :param keys: List of strings corresponding to keys stored in the
        datastore. If None or an empty list then return all keys.
        :type keys: dict or None
        :return: A dictionary where keys correspond to datastore keys and
        values are lists of strings associated with each key in the datastore.
        If a requested key is missing, use None for its return value.
        :rtype: dict
        """
        raise NotImplementedError

    @abc.abstractmethod
    def update(self, keys):
        """
        Update an existing key in the datastore.

        :param dict keys: Dictionary where keys correspond to datastore keys
        and values are lists of strings to be added to the given key's existing
        list of values. If a given key doesn't already exist in the datastore,
        then add it.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def delete(self, keys):
        """
        Remove the specified keys and their associated values from the
        datastore.

        :param list keys: List of keys to remove from the datastore.
        """
        raise NotImplementedError
