# -*- coding:utf-8 -*-
"""Defines datastore-specific exceptions."""

from nce.errors import KVStoreError


class InvalidDataStoreError(KVStoreError):
    pass
