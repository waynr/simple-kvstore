# -*- coding: utf-8 -*-
"""Application configuration."""
import os

from nce.datastore.dynamodb import DynamoDataStore


class Config(object):
    """Base configuration."""

    SECRET_KEY = os.environ.get('NCE_SECRET', 'secret-key')
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))

    AWS_REGION_NAME = 'us-west-2'
    DYNAMODB_TABLENAME = 'kvstore'
    DYNAMODB_LOCAL_PORT = '8000'

    # Assign a concrete DataStore class
    DATASTORE_CLASS = DynamoDataStore

    # Choose which type of http service to use.
    HTTP_SERVICE = os.environ.get("HTTP_SERVICE", "local-flask")


class ProdConfig(Config):
    """Production configuration."""

    ENV = 'prod'
    DEBUG = False
    DYNAMODB_TABLENAME = os.environ.get("DYNAMODB_TABLENAME", 'kvstore')


class DevConfig(Config):
    """Development configuration."""

    ENV = 'dev'
    DEBUG = True
    if os.environ.get("USE_AWS_DYNAMODB", "false").lower() == "false":
        DYNAMODB_LOCAL = True


class TestConfig(DevConfig):
    """Test configuration."""

    ENV = 'test'
    TESTING = True
    DYNAMODB_TABLENAME = 'kvstore-test'
