# -*- coding: utf-8 -*-
"""Public flask views."""

from flask import Blueprint
from flask import current_app as app
from flask import jsonify
from flask import request

from nce.datastore.factory import DataStoreFactory

blueprint = Blueprint('api-v1', __name__,
                      url_prefix='/api/v1')


@blueprint.route('/key', methods=['POST', 'GET'])
def keys(event=None, context=None):
    """HTTP endpoint intended to address the set of keyvalues as a whole, ie by
    adding new values or retrieving all values.
    """
    datastore = DataStoreFactory.create_from_flask_config(app.config)

    if request.method == "POST":
        datastore.add(request.get_json())
        return "Successful addition."
    else:
        result = datastore.get()
        return jsonify(result)


@blueprint.route('/key/<string:name>', methods=['DELETE', 'PUT', 'GET'])
def key(name, event=None, context=None):
    """HTTP endpoint intended to perform operations on individual keyvalues.
    """
    datastore = DataStoreFactory.create_from_flask_config(app.config)

    if request.method == "PUT":
        datastore.update(request.get_json())
        return "Successful update."
    elif request.method == "DELETE":
        datastore.delete([name])
        return "Successful deletion."
    else:
        result = datastore.get([name])
        return jsonify(result)
