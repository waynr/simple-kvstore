# -*- coding: utf-8 -*-
"""Public flask views."""

from flask import Blueprint

blueprint = Blueprint('api', __name__,
                      url_prefix='/api')


@blueprint.route('/', methods=['GET'])
def api_index(event=None, context=None):
    return "Hello World!"
