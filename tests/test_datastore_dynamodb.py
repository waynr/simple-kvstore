#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" This module tests the DynamoDataStore object. The set of cases validated
here is currently severly limited due to limitations on the amount of time I
can dedicate to fully testing it.
"""


def test_add_keyvalue(datastore, dynamodb_table):
    """Validate the it is possible to add a value to dynamodb using the
    DynamoDataStore class.
    """

    kvs = {
        'cat': ['meow', 'mew', 'miaow']
    }

    # add the kvs to the datastore
    datastore.add(kvs)

    # validate the dynamodb item by checking the dynamodb table directly
    ddb_entry = dynamodb_table.get_item(Key={'key': 'cat'})['Item']

    assert sorted(kvs[ddb_entry['key']]) == sorted(ddb_entry['value'])


def test_update_keyvalue(datastore, dynamodb_table):
    """Validate that DynamoDataStore class can update a key's values in place.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {
        'cat': ['meow', 'mew', 'miaow']
    }
    for k, v in kvs.items():
        dynamodb_table.put_item(Item={'key': k,
                                      'value': v})

    new_kvs = {
        'cat': ['mrow']
    }

    # update the kvs to the datastore
    datastore.update(new_kvs)

    combined_kvs = {}
    ddb_entries = []
    for k in kvs.keys():
        combined_kvs[k] = kvs[k] + new_kvs[k]
        ddb_entries.append(dynamodb_table
                           .get_item(Key={'key': 'cat'})['Item'])

    # validate the dynamodb item by checking the dynamodb table directly
    for ddb_entry in ddb_entries:
        assert (sorted(combined_kvs[ddb_entry['key']]) ==
                sorted(ddb_entry['value']))


def test_remove_keyvalue(datastore, dynamodb_table):
    """Validate that DynamoDataStore class can remove a kv pair from the
    kvstore.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {
        'cat': ['meow', 'mew', 'miaow']
    }

    for k, v in kvs.items():
        # add the key to dynamodb
        dynamodb_table.put_item(Item={'key': k,
                                      'value': v})

        # delete the key from dynamodb
        datastore.delete([k])

        # boto3 dynamodb resource request for the specified item should return
        # a response (dict) without an 'Item' key.
        ddb_entry = dynamodb_table.get_item(Key={'key': k}).get('Item',
                                                                None)
        assert ddb_entry is None


def test_get_specific_keyvalue(datastore, dynamodb_table):
    """Test that we can get a specific keyvalue using DynamoDataStore.
    """
    kvs = {
        'cat': ['meow', 'mew', 'miaow']
    }

    for k, v in kvs.items():
        dynamodb_table.put_item(Item={'key': k,
                                      'value': v})

    for k, v in kvs.items():
        actual_value = datastore.get([k])
        assert sorted(v) == sorted(actual_value[k])


def test_get_all_keyvalue(datastore, dynamodb_table):
    """Test that we can get all keyvalue using DynamoDataStore.
    """
    kvs = {
        'cat': ['meow', 'mew', 'miaow'],
        'dog': ['woff', 'wuf', 'grr']
    }

    for k, v in kvs.items():
        dynamodb_table.put_item(Item={'key': k,
                                      'value': v})

    actual_value = datastore.get()
    for k, v in kvs.items():
        assert sorted(v) == sorted(actual_value[k])
