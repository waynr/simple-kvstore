#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy


def test_hello_world(testapp):
    """Test that <url>/api returns plaintext "Hello World!". This test will be
    removed at a later datetime but is useful just to validate that all the
    Flask bits are in place for an initial version of this repository.
    """
    result = testapp.get('/api/')

    assert result.status_code == 200
    assert "Hello World!" in result


def test_add_keyvalue(testapp, datastore):
    """Test adding a key to the kvstore app.
    """
    kvs = {"cat": ['meow', 'mew', 'miaow']}

    response = testapp.post_json('/api/v1/key', kvs)

    # Should get HTTP 200 response.
    assert response.status_code == 200

    # Should be able to retrieve the data we put in through http api directly
    # from the datastore.
    for k, v in kvs.items():
        ds_entry = datastore.get([k])
        ds_entry[k] = sorted(ds_entry[k])
        assert {k: sorted(v)} == ds_entry


def test_update_keyvalue(testapp, datastore):
    """Test updating an exiting key in the kvstore app.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {"cat": ['meow', 'mew', 'miaow']}
    new_kvs = {"cat": ["mrow"]}

    # Add new value to key using PUT request
    for k, v in new_kvs.items():
        datastore.add(kvs)
        response = testapp.put_json('/api/v1/key/{0}'.format(k), {k: v})

        # Should get HTTP 200 response
        assert response.status_code == 200

        # Should be able to retrieve the original keyvalue pair along with the
        # additional value put in through the http api.
        actual_kvs = datastore.get([k])
        expected_kvs = {copy.copy(k): copy.deepcopy(v)}
        expected_kvs[k].extend(kvs[k])
        assert sorted(actual_kvs[k]) == sorted(expected_kvs[k])

        datastore.delete(k)


def test_remove_keyvalue(testapp, datastore):
    """Test removing a key from the kvstore app.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {"cat": ['meow', 'mew', 'miaow']}
    datastore.add(kvs)

    # Remove the value using a DELETE request
    response = testapp.delete('/api/v1/key/{0}'.format("cat"))

    # Should get HTTP 200 response
    assert response.status_code == 200

    # boto3 dynamodb resource request for the specified item should return a
    # response (dict) without an 'Item' key.
    ddb_entry = datastore.get(["cat"])
    assert ddb_entry["cat"] is None


def test_get_specific_keyvalue(testapp, datastore):
    """Test retrieval of a specific key from the kvstore app.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {"cat": ["meow", "mew", "miaow"]}
    datastore.add(kvs)

    for k, v in kvs.items():
        # GET value from http service
        response = testapp.get('/api/v1/key/{0}'.format(k))

        # Should get HTTP 200 response
        assert response.status_code == 200

        # Validate using sorted values since the API doesn't currently define
        # any guarantees about ordering of values.
        assert sorted(kvs[k]) == sorted(response.json[k])


def test_get_all_keyvalue(testapp, datastore):
    """Test adding a key to the kvstore app.
    """
    # Set up by adding initial keyvalue data directly to dynamodb.
    kvs = {"cat": ['meow', 'mew', 'miaow'],
           "dog": ['ruff', 'woof', 'wuff', 'bark', 'yip']}
    datastore.add(kvs)

    # GET value from http service
    response = testapp.get('/api/v1/key')

    # Should get HTTP 200 response
    assert response.status_code == 200

    # Validate HTTP body matches value expected based on the data put into data
    # store. Do this key-by-key.
    actual_value = response.json
    for k, v in kvs.items():
        # Validate using sorted values since the API doesn't currently define
        # any guarantees about ordering of values.
        assert sorted(actual_value[k]) == sorted(v)
