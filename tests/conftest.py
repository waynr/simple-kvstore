#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import urllib.parse

import botocore
import docker
import pytest
import requests
from flask import Config
from webtest import TestApp
from wsgiproxy import HostProxy
from zappa.cli import ZappaCLI

from nce.app import create_app
from nce.datastore.factory import DataStoreFactory
from nce.errors import ConfigurationError
from nce.settings import TestConfig


@pytest.yield_fixture(scope='session')
def session_setup_teardown():
    """ Allow session-scoped fixtures that are used in a nonstandard way to be
    cleaned up while also yielding a dictionary that can be used to store such
    fixture-scoped fixtures.
    """
    yields = {}

    yield yields

    for name, fixture in yields.items():
        print("tearing down {0}".format(name))

        try:
            next(fixture['generator'])
        except StopIteration:
            pass


@pytest.yield_fixture(scope='session')
def dynamodb_container():
    """A running dynamodb docker container."""
    # Obtain docker-py client object. Since docker-py communicates directly
    # with the docker daemon it needs to know what api version to use. The best
    # way to determine this as far as I know is to ask the docker client.
    #
    # Incidentally, I think maybe the subprocess->check_output->rstrip->decode
    # chain of calls meets the requirement that this coding exercise
    # demonstrate the use of chained method calls, ie "chain (make a call the
    # result of which informs a subsequent call)" although that wording is
    # worryingly vague.
    dv_command = "docker version --format \'{{.Server.APIVersion}}\'"
    docker_api_version = subprocess.check_output(dv_command, shell=True)\
                                   .rstrip()\
                                   .decode('UTF-8')
    client = docker.from_env(version=docker_api_version)

    # Need to pull down the image manually since the client.containers.create()
    # method doesn't pull images before attempting to create a container. Which
    # is stupid.
    image_name = "bigtruedata/dynamo:8-jre"
    client.images.pull(image_name)

    # Create and run detached container. Map dynamodb port to port 8001 on dev
    # host.
    container = client.containers.run(
        image_name,
        detach=True,
    )

    yield container

    # Stop and remove the container but make sure nothing bad happens mmmkay.
    name = container.short_id
    force_remove = False
    try:
        container.stop()
    except requests.exceptions.ReadTimeout:
        print("Failed to stop {0} container.".format(name))
        force_remove = True

    container.remove(force=force_remove)


@pytest.yield_fixture(scope='function')
def flask_app(session_setup_teardown):
    """A Flask application for use in tests."""

    ddb_fixture = session_setup_teardown.get("dynamodb_container", None)
    if ddb_fixture is None:
        ddb_generator = dynamodb_container()
        ddb_container = next(ddb_generator)
        session_setup_teardown["dynamodb_container"] = {
            "generator": ddb_generator,
            "container": ddb_container
        }
    else:
        ddb_container = ddb_fixture['container']

    ip_command = ('docker inspect -f \'{{.NetworkSettings.IPAddress}}\' %s '
                  % ddb_container.id)
    TestConfig.DYNAMODB_LOCAL_HOST = subprocess.check_output(ip_command,
                                                             shell=True)\
                                               .rstrip()\
                                               .decode('UTF-8')

    _app = create_app(TestConfig)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture(scope='session')
def zappa_cli():
    zappa = ZappaCLI()
    zappa.stage_env = "pytest"
    zappa.api_stage = "pytest"
    zappa.load_settings(os.environ["ZAPPA_SETTINGS"])
    return zappa


@pytest.yield_fixture(scope='session')
def lambda_package(zappa_cli):
    # Create lambda zip
    zappa_cli.create_package()

    # Upload it to S3
    success = zappa_cli.zappa.upload_to_s3(zappa_cli.zip_path,
                                           zappa_cli.s3_bucket_name,
                                           disable_progress=True)
    if not success:
        raise Exception("Unable to upload to S3. Quitting.")

    yield None

    zappa_cli.remove_local_zip()
    zappa_cli.remove_uploaded_zip()


@pytest.yield_fixture(scope='session')
def lambda_app():
    """An AWS lambda test fixture that sets up and tears down a live AWS lambda
    environment against which to test and yields the URL of that environment
    for outer contexts to use.

    Much of the code in this function is heavily inspired by the code behind
    the 'zappa deploy` and `zappa undeploy` subcommands, modified for use
    outside the ZappaCLI class and within this specific context.
    """
    zappa = zappa_cli()
    lambda_g = lambda_package(zappa)
    next(lambda_g)

    role_exists = False
    while not role_exists:
        try:
            zappa.zappa.create_iam_roles()
        except botocore.errorfactory.InvalidParameterValueException:
            continue
        role_exists = True

    # Register the Lambda function with that zip as the source
    # You'll also need to define the path to your lambda_handler code.
    zappa.lambda_arn = zappa.zappa.create_lambda_function(
        bucket=zappa.s3_bucket_name,
        s3_key=zappa.zip_path,
        function_name=zappa.lambda_name,
        handler=zappa.lambda_handler,
        description=zappa.lambda_description,
        vpc_config=zappa.vpc_config,
        dead_letter_config=zappa.dead_letter_config,
        timeout=zappa.timeout_seconds,
        memory_size=zappa.memory_size,
        runtime=zappa.runtime,
        aws_environment_variables={
            "DYNAMODB_TABLENAME": TestConfig.DYNAMODB_TABLENAME
        },
        aws_kms_key_arn=zappa.aws_kms_key_arn
    )

    # schedule events for this lambda
    zappa.schedule()

    # Create and configure the API Gateway
    zappa.zappa.create_stack_template(
        lambda_arn=zappa.lambda_arn,
        lambda_name=zappa.lambda_name,
        api_key_required=zappa.api_key_required,
        iam_authorization=zappa.iam_authorization,
        authorizer=zappa.authorizer,
        cors_options=zappa.cors,
        description=zappa.apigateway_description
    )

    zappa.zappa.update_stack(
        zappa.lambda_name,
        zappa.s3_bucket_name,
        wait=True,
        disable_progress=True,
    )

    api_id = zappa.zappa.get_api_id(zappa.lambda_name)
    zappa.zappa.add_binary_support(api_id=api_id, cors=zappa.cors)
    endpoint_url = zappa.deploy_api_gateway(api_id)

    yield endpoint_url

    zappa.tail(
        colorize=False,
        keep_open=False,
        http=True,
        non_http=True,
        since='200s',
        filter_pattern='',
    )

    zappa.zappa.remove_api_gateway_logs(zappa.lambda_name)
    zappa.unschedule()  # removes event triggers, including warm up event.
    zappa.zappa.delete_lambda_function(zappa.lambda_name)

    for generator in [lambda_g]:
        try:
            next(generator)
        except StopIteration:
            pass


class FlaskTestApp(TestApp):
    """
    A custom TestApp intended that contains a flask.Config attribute. Intended
    to provide a uniform approach to accessing flask configs between Flask and
    Lambda testing.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config = self.app.config


class RawURIProxy(HostProxy):

    def __init__(self, url, client):
        super().__init__(url, client)
        self.__raw_url = url

    def extract_uri(self, environ):
        parsed = urllib.parse.urlparse(self.__raw_url)
        environ['SCHEME'] = parsed.scheme
        environ['HTTP_HOST'] = parsed.netloc
        return self.__raw_url.strip('/')


class LambdaTestApp(TestApp):
    """
    A custom TestApp intended that contains a flask.Config attribute. Intended
    to provide a uniform approach to accessing flask configs between Flask and
    Lambda testing.

    Also sets the 'app' attribute to use the custom RawURIProxy.
    """

    def __init__(self, url, config_obj):
        super().__init__(url)
        self.config = Config(root_path=os.path.dirname(
            os.path.dirname(__file__)))
        self.config.from_object(config_obj)
        self.app = RawURIProxy(url, client='httplib')


@pytest.fixture(scope='function')
def testapp(session_setup_teardown):
    """WebTest fixture that inspects the TestConfig object to determine whether
    to return a flask or a lambda test fixture.
    """
    if not hasattr(TestConfig, "HTTP_SERVICE"):
        raise ConfigurationError("Missing config value 'HTTP_SERVICE'")

    http_service = TestConfig.HTTP_SERVICE

    if http_service == "local-flask":
        app_generator = flask_app(session_setup_teardown)

        yield FlaskTestApp(next(app_generator))
        try:
            next(app_generator)
        except StopIteration:
            pass
    elif http_service == "aws-lambda":
        lambda_fixture = session_setup_teardown.get("lambda_app", None)
        if lambda_fixture is None:
            app_generator = lambda_app()
            url = next(app_generator)
            session_setup_teardown['lambda_app'] = {
                "generator": app_generator,
                "url": url,
            }
        else:
            url = lambda_fixture['url']

        yield LambdaTestApp(url, TestConfig)
    else:
        msg = ("{0} is and invalid HTTP_SERVICE value, please "
               "specify either 'local-flask' or 'aws-lambda'")
        raise ConfigurationError(msg.format(http_service))


@pytest.yield_fixture(scope='function')
def datastore(testapp):
    ds = DataStoreFactory.create_from_flask_config(testapp.config)
    ds.setup()

    yield ds

    ds.teardown()


@pytest.fixture(scope='function')
def dynamodb_table(datastore):
    """A dynamodb docker container."""
    return datastore.ddb_table
