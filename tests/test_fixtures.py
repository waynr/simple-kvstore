#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess


def test_dynamodb_docker_container_fixture_runs(dynamodb_container):
    """Test that the dynamodb docker container is actually running. This is
    done in isolation from functional tests against the coding exercise app to
    isolate it as a potential source of failure during a given test run.
    """
    status_command = ("docker inspect -f \'{{.State.Status}}\' %s "
                      % dynamodb_container.id)
    dynamodb_status = subprocess.check_output(status_command, shell=True)\
                                .rstrip()\
                                .decode('UTF-8')

    assert dynamodb_status == 'running'


def test_dynamodb_table_fixture_tables(dynamodb_table):
    # put some data into the kvstore table
    example_data = {
        "key": "sports",
        "value": {"dodgeball", "bocci", "freestyle",
                  "ultimate", "surfing", "judo"}
    }
    dynamodb_table.put_item(Item=example_data)

    # see if it's there
    retrieved_data = dynamodb_table.get_item(Key={"key": "sports"})['Item']
    assert example_data == retrieved_data
